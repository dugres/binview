########
binview
########

**Binary dumper**

========
Abstract
========

Dumps binary files to stdout.

=====
Usage
=====

.. code-block:: shell

    binview FILE


============
Installation
============

.. code-block:: shell 

    $ pip install binview
